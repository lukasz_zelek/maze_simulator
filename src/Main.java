import java.awt.Dimension;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferStrategy;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Vector;
import javax.swing.*;
public class Main implements KeyListener{

    public static final ImageIcon pin = new ImageIcon("img/pin.png");
    public static final ImageIcon wallh = new ImageIcon("img/wallh.png");
    public static final ImageIcon wallw = new ImageIcon("img/wallw.png");
    public static final ImageIcon wallhb = new ImageIcon("img/wallhb.png");
    public static final ImageIcon wallwb = new ImageIcon("img/wallwb.png");
    public static final ImageIcon mouseN = new ImageIcon("img/mousen.gif");
    public static final ImageIcon mouseS = new ImageIcon("img/mouses.gif");
    public static final ImageIcon mouseE = new ImageIcon("img/mousee.gif");
    public static final ImageIcon mouseW = new ImageIcon("img/mousew.gif");
    public static final ImageIcon bg = new ImageIcon("img/bg.gif");
    public static final int WIDTH = 1600;
    public static final int HEIGHT = 1024;
    public static final Vector<Dimension> PATH = new Vector<Dimension>();
    public static int mapingLength = 0;
    public static final Vector<Dimension> CPATH = new Vector<Dimension>();
    public static Color colorPath = new Color(0, 126, 255);
    public static Color colorCorrectPath = new Color(255, 121, 0);
    static ArrayList<String> bufor = new ArrayList<String>();
    Thread map = new Thread(new Runnable() {
        @Override
        public synchronized void run(){
                mm.maping();
                simulation = false;
                bufor.add("Maping: " + mapingLength + " edges");
                bufor.add("Final Way: " + Integer.toString(PATH.size()) + " edges");
                bufor.add("simulation > ");
                paintScene();
        }
    });
    public static int coordX = 40, coordY = 43;
    static int wX = 3, wY = 60;
    static BufferStrategy bs;
    JFrame okno;
    static char maze[][] = new char[33][33];
    static MicroMouse mm = null;
    static boolean simulation = false;
            Scanner input;
    static int corrections[] = {-15,2,17,0};
    public static final Image kratki[] = new Image[16];
    public static final int kratkicoord[] = {0,4,2,6,1,5,3,7,8,12,10,14,9,13,11,15};

    public Main()
    {
        okno = new JFrame("Micromouse In Maze Algorithm Simulator");
        okno.setBounds(0, 0, WIDTH, HEIGHT);
        okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        okno.setVisible(true);
        okno.createBufferStrategy(2);
        okno.setResizable(false);
        okno.setIgnoreRepaint(true);
        bs = okno.getBufferStrategy();
        okno.addKeyListener(this);
        bufor.add("Welcome in MIMAS! To get instructions type \"help\".");
        bufor.add("simulation > ");
        for(int i = 0; i <= 0xe; i++)
            kratki[i] = new ImageIcon("img/"+i+".png").getImage();
    }

    public void readMaze(String path, int type) throws FileNotFoundException
    {
        File plik = new File(path);
        if(type == 0) {
            input = new Scanner(plik);
            int nr_linii = 0;
            while (input.hasNext()) {
                String linia = input.nextLine();
                for (int i = 0; i < 33; i++) {
                    maze[nr_linii][i] = linia.charAt(i);
                }
                nr_linii++;
            }
        }
        else {
            for (int i = 0; i < 33; i++)
                for (int j = 0; j < 33; j++)
                    maze[i][j] = 'o';
            for (int i = 0; i < 33; i += 2) {
                for (int j = 0; j < 33; j += 2)
                    maze[i][j] = '.';
            }
            for (int j = 0; j < 33; j += 32) {
                for (int i = 1; i < 33; i += 2)
                    maze[i][j] = '*';
            }
            for (int i = 0; i < 33; i += 32) {
                for (int j = 1; j < 33; j += 2)
                    maze[i][j] = '#';
            }
            plik = new File(path);
            input = new Scanner(plik);
            int nr_linii = 0;
            while (input.hasNext()) {
                String linia = input.nextLine();
                int j = 0, k = 0;
                for (int i = 0; i < linia.length(); i++) {
                    if (linia.charAt(i) != ' ') {
                        k++;
                        //8 -zachód
                        //4 -północ
                        //2 - wschód
                        //1 - południe
                        if (k % 2 == 0) {
                            int maska = 0;
                            switch (linia.charAt(i)) {
                                case '0':
                                    maska = 0;
                                    break;
                                case '1':
                                    maska = 1;
                                    break;
                                case '2':
                                    maska = 2;
                                    break;
                                case '3':
                                    maska = 3;
                                    break;
                                case '4':
                                    maska = 4;
                                    break;
                                case '5':
                                    maska = 5;
                                    break;
                                case '6':
                                    maska = 6;
                                    break;
                                case '7':
                                    maska = 7;
                                    break;
                                case '8':
                                    maska = 8;
                                    break;
                                case '9':
                                    maska = 9;
                                    break;
                                case 'a':
                                    maska = 10;
                                    break;
                                case 'b':
                                    maska = 11;
                                    break;
                                case 'c':
                                    maska = 12;
                                    break;
                                case 'd':
                                    maska = 13;
                                    break;
                                case 'e':
                                    maska = 14;
                                    break;
                            }
                            if ((maska & (1 << 3)) == (1 << 3)) {
                                maze[2 * j + 1][2 * nr_linii] = '|';
                            }
                            if ((maska & (1 << 2)) == (1 << 2)) {
                                maze[2 * j][2 * nr_linii + 1] = '-';
                            }
                            if ((maska & (1 << 1)) == (1 << 1)) {
                                maze[2 * j + 1][2 * nr_linii + 2] = '|';
                            }
                            if ((maska & 1) == 1) {
                                maze[2 * j + 2][2 * nr_linii + 1] = '-';
                            }
                            j++;
                        }
                    }
                }
                nr_linii++;
            }
        }
        maze[16][16] = 'o';
        bestRoad();
    }

    public static int xOnScreen(int X)
    {
        X = (X-1)%16;
        return coordX + wY * X + wY/2;
    }

    public static int yOnScreen(int Y)
    {
        Y = (Y-1)/16;
        return coordY + wY * Y + wY/2;
    }

    public static void paintPath(Graphics2D g)
    {
        g.setColor(colorPath);
        for(Dimension p : PATH)
        {
            switch (p.width - p.height)
            {
                case 1:
                    g.fillRect(xOnScreen(p.height),yOnScreen(p.height),wY,wX);
                break;
                case -1:
                    g.fillRect(xOnScreen(p.width),yOnScreen(p.width),wY,wX);
                    break;
                case 16:
                    g.fillRect(xOnScreen(p.height),yOnScreen(p.height),wX,wY+3);
                    break;
                case -16:
                    g.fillRect(xOnScreen(p.width),yOnScreen(p.width),wX,wY+3);
                    break;
            }
        }
    }

    public static void paintCorrectPath(Graphics2D g)
    {
        g.setColor(colorCorrectPath);
        for(Dimension p : CPATH)
        {
            switch (p.width - p.height)
            {
                case 1:
                    g.fillRect(xOnScreen(p.height),yOnScreen(p.height),wY,wX);
                    break;
                case -1:
                    g.fillRect(xOnScreen(p.width),yOnScreen(p.width),wY,wX);
                    break;
                case 16:
                    g.fillRect(xOnScreen(p.height),yOnScreen(p.height),wX,wY+3);
                    break;
                case -16:
                    g.fillRect(xOnScreen(p.width),yOnScreen(p.width),wX,wY+3);
                    break;
            }
        }
    }

    public static void paintConsole(Graphics2D g)
    {
        g.setColor(new Color(100, 214, 54));
        g.setFont(new Font("Consolas",Font.BOLD,16));
        for(int i = 0; i < Math.min(22,bufor.size()); i++) {
            String s = bufor.get(Math.max(i,bufor.size()-22+i));
            g.drawString(s, HEIGHT + 5, 580 + i * 20);
        }
    }

    private static void paintMappedAreaAndFlooding(Graphics2D g)
    {
        g.setColor(new Color(103, 98, 106));
        g.setFont(new Font("Consolas",Font.BOLD,20));
        int sk = 32, crdX = 36, crdY = 55;
        int mappedArea[][] = mm.getMappedArea();
        int flooding[][] = mm.getFlooding();
        int at = mm.getAreaType();
        if(at != -1)
            for(int i = 0; i < 16; i++)
                for(int j = 0; j < 16; j++)
                {
                    int k;
                    if(at==1)k=15-j;
                    else k = j;
                    /*
                    if(mappedArea[i][j] > 9)
                    {
                        switch (mappedArea[i][j])
                        {
                            case 10:g.drawString("A", HEIGHT + crdX + k * sk, crdY + i * sk);break;
                            case 11:g.drawString("B", HEIGHT + crdX + k * sk, crdY + i * sk);break;
                            case 12:g.drawString("C", HEIGHT + crdX + k * sk, crdY + i * sk);break;
                            case 13:g.drawString("D", HEIGHT + crdX + k * sk, crdY + i * sk);break;
                            case 14:g.drawString("E", HEIGHT + crdX + k * sk, crdY + i * sk);break;
                        }
                    }
                    else
                        g.drawString(Integer.toString(mappedArea[i][j]), HEIGHT + crdX + k * sk, crdY + i * sk);
                    */
                    if(mappedArea[i][j] != -1)
                        g.drawImage(kratki[kratkicoord[mappedArea[i][j]]],HEIGHT + crdX + k * 30, crdY + i * 30,30,30,null);
                    g.setColor(new Color(255, 255, 255));
                    if(at == 1)g.drawString(Integer.toString(flooding[j][i]), 25+crdX + j * wY, 25+crdY + i * wY);
                    else g.drawString(Integer.toString(flooding[i][j]), 25+crdX + j * wY, 25+crdY + i * wY);
                    g.setColor(new Color(103, 98, 106));
                }
    }

    public static void paintScene()
    {
        Graphics2D g = (Graphics2D) bs.getDrawGraphics();
        g.drawImage(bg.getImage(), 0, 0, null);
        g.setColor(Color.BLACK);
        g.fillRect(HEIGHT,30,WIDTH-HEIGHT,556);
        g.setColor(new Color(48, 10, 36));
        g.fillRect(HEIGHT,556,WIDTH-HEIGHT,HEIGHT-556);
        paintConsole(g);
        g.setColor(Color.RED);

        for (int i = 0; i <= 32; i += 2) {
            for (int j = 1; j < 33; j += 2) {
                if (maze[i][j] == '-' || maze[i][j] == '#') {
                    g.drawImage(wallhb.getImage(), coordX + (j / 2) * wY + 2, coordY + (i / 2) * wY, wY, wX, null);
                }
            }
        }
        for (int i = 1; i < 33; i += 2) {
            for (int j = 0; j <= 32; j += 2) {
                if (maze[i][j] == '|' || maze[i][j] == '*') {
                    g.drawImage(wallwb.getImage(), coordX + (j / 2) * wY, coordY + (i / 2) * wY + 2, wX, wY, null);
                }
            }
        }
        for (int i = 0; i < 33; i += 2) {
            for (int j = 0; j < 33; j += 2) {
                if (maze[i][j] == '.') {
                    g.drawImage(pin.getImage(), coordX + (i / 2) * wY, coordY + (j / 2) * wY, wX, wX, null);
                }
            }
        }
        paintCorrectPath(g);
        paintPath(g);
        if(mm != null)
            switch (mm.getRotation()) {
                case 0:
                    g.drawImage(mouseN.getImage(), coordX + mm.getX() * wY, coordY + mm.getY() * wY, null);
                    break;
                case 1:
                    g.drawImage(mouseE.getImage(), coordX + mm.getX() * wY, coordY + mm.getY() * wY, null);
                    break;
                case 2:
                    g.drawImage(mouseS.getImage(), coordX + mm.getX() * wY, coordY + mm.getY() * wY, null);
                    break;
                case 3:
                    g.drawImage(mouseW.getImage(), coordX + mm.getX() * wY, coordY + mm.getY() * wY, null);
                    break;
            }
        if(mm!=null)paintMappedAreaAndFlooding(g);
        bs.show();
    }

    void bestRoad()
    {
        CPATH.clear();
        Vector<Vector<Integer>> GRAPH = new Vector<Vector<Integer>>();
        for(int i = 0; i <= 0xff; i++)
            GRAPH.add(new Vector<Integer>());
        for(int i = 1; i < 32 ; i+=2)
        {
            for(int j = 1; j < 32; j+=2)
            {
                if (maze[i - 1][j + 1] == 'o') {
                    GRAPH.get((i-1)*8+j/2).add((i-1)*8+j/2-16);
                }
                if (maze[i][j + 1] == 'o') {
                    GRAPH.get((i-1)*8+j/2).add((i-1)*8+j/2+1);
                }
                if (maze[i + 1][j] == 'o') {
                    GRAPH.get((i-1)*8+j/2).add((i-1)*8+j/2+16);
                }
                if (maze[i][j - 1] == 'o') {
                    GRAPH.get((i-1)*8+j/2).add((i-1)*8+j/2-1);
                }
            }
        }

        int d[] = new int[0xff + 1];
        int p[] = new int[0xff + 1];
        for(int i = 0; i <= 0xff; i++)
        {
            d[i] = 1000;
            p[i] = -1;
        }

        int tab[] = {0x77, 0x78, 0x87, 0x88};
        for(int i = 0; i < 4; i++)
            d[tab[i]] = 0;
        for(int i = 0; i < 0xff; i++)
            for(int u = 0; u <= 0xff;u++)
                for(int v = 0; v < GRAPH.get(u).size(); v++)
                {
                    if (d[GRAPH.get(u).get(v)] > d[u] + 1)
                    {
                        d[GRAPH.get(u).get(v)] = d[u] + 1;
                        p[GRAPH.get(u).get(v)] = u;
                    }
                    if (d[u] > d[GRAPH.get(u).get(v)] + 1)
                    {
                        d[u] = d[GRAPH.get(u).get(v)] + 1;
                        p[u] = GRAPH.get(u).get(v);
                    }
                }
        int k = 0;
        while(k != tab[0] && k != tab[1] && k != tab[2] && k != tab[3])
        {
            CPATH.add(new Dimension(k+1,p[k]+1));
            k = p[k];
        }
    }

    public static void main(String args[])throws FileNotFoundException
    {
        Main m = new Main();
        m.readMaze("mazes/maze-default.txt",0);
        m.paintScene();
    }

    @Override
    public void keyPressed(KeyEvent e){
        switch (e.getKeyCode()) {
            case KeyEvent.VK_UP:
                if(mm!=null)
                    if(!mm.isWallFront()) {
                        PATH.add(new Dimension(mm.getY()*16 + mm.getX() + 1,mm.getY()*16 + mm.getX() + corrections[mm.rotation]));
                        mm.goForward();
                    }
                break;
            case KeyEvent.VK_LEFT:
                if(mm!=null)mm.turnLeft();
                break;
            case KeyEvent.VK_RIGHT:
                if(mm!=null)mm.turnRight();
                break;
            case KeyEvent.VK_BACK_SPACE:
                if(bufor.get(bufor.size()-1).length() > 13)
                {
                    bufor.set(bufor.size()-1,bufor.get(bufor.size()-1).substring(0,bufor.get(bufor.size()-1).length()-1));
                }
                break;
            case KeyEvent.VK_PAGE_UP:
                break;
            case KeyEvent.VK_PAGE_DOWN:
                break;
            case KeyEvent.VK_ENTER:
                String buf = bufor.get(bufor.size()-1);
                int idx = buf.length();
                ArrayList<Integer> params = new ArrayList<Integer>();
                for(int i = 13; i < buf.length(); i++){
                    if(Character.isDigit(buf.charAt(i)))
                        params.set(params.size()-1,params.get(params.size()-1)*10 +Character.getNumericValue(buf.charAt(i)));
                    if(buf.charAt(i) == ' ') {
                        params.add(0);
                        if(idx == buf.length())
                            idx = i;
                    }
                }
                buf = buf.substring(0,idx);
                if(buf.length() > 13) {

                    if (buf.substring(13, buf.length()).equals("cls")) {
                        bufor.clear();
                    } else if (buf.substring(13, buf.length()).equals("sta")) {

                    }
                    else if (buf.substring(13, buf.length()).equals("stp"))
                    {
                        simulation = false;
                    }
                    else if (buf.substring(13, buf.length()).equals("stm"))
                    {
                        if(params.size() == 1)
                            mm.setTimeout(params.get(0));
                        else
                            bufor.add("bad parameters");

                    }
                    else if (buf.substring(13, buf.length()).equals("map"))
                    {
                        if(mm!=null)
                        {
                            simulation = true;
                            map.start();
                        }
                        else
                            bufor.add("set micromouse first");
                    }
                    else if (buf.substring(13, buf.length()).equals("clr")) {
                        mm = null;
                        PATH.clear();
                        map = new Thread(new Runnable() {
                            @Override
                            public synchronized void run() {
                                mm.maping();
                                simulation = false;
                                bufor.add("Maping: " + mapingLength + " edges");
                                bufor.add("Final Way: " + Integer.toString(PATH.size()) + " edges");
                                bufor.add("simulation > ");
                                paintScene();
                            }
                        });
                    }
                    else if (buf.substring(13, buf.length()).equals("help")) {
                        bufor.add("set -> set micromouse(x,y,rot)(example \"set 0 0 3\")");
                        bufor.add("map -> map maze");
                        bufor.add("stm -> set timeout");
                        bufor.add("sta -> start simulation");
                        bufor.add("stp -> stop simulation");
                        bufor.add("clr -> clear path in maze");
                        bufor.add("cls -> clear screen");
                        bufor.add("maz -> change maze map");
                        bufor.add("avm -> show available mazes");
                        bufor.add("ext -> close program");
                    }
                    else if (buf.substring(13, buf.length()).equals("set")) {
                        if(params.size()==3)
                            mm = new MicroMouse(params.get(0), params.get(1), params.get(2),maze);
                        else
                            bufor.add("bad parameters");
                    }
                    else if (buf.substring(13, buf.length()).equals("avm")) {
                        bufor.add("0 1 2 100 2000 2001 2002 2003 2004 2005 2007 2008 2009");
                        bufor.add("2011 2018");
                    }
                    else if (buf.substring(13, buf.length()).equals("ext")) {
                        System.exit(0);
                    }
                    else if (buf.substring(13, buf.length()).equals("leg")) {
                        bufor.add("0    ");
                        bufor.add("1  ' ");
                        bufor.add("2   |");
                        bufor.add("3  '|");
                        bufor.add("4  , ");
                        bufor.add("5  : ");
                        bufor.add("6  ,|");
                        bufor.add("7  :|");
                        bufor.add("8 |  ");
                        bufor.add("9 |' ");
                        bufor.add("A | |");
                        bufor.add("B |'|");
                        bufor.add("C |, ");
                        bufor.add("D |: ");
                        bufor.add("E |,|");
                    }
                    else if (buf.substring(13, buf.length()).equals("maz")) {
                        if(params.size()==1)
                        try {
                            int typus = 1;
                            if(params.get(0) == 2018 || params.get(0) == 1 || params.get(0) == 2)
                                typus = 0;
                            readMaze("maz/"+params.get(0)+".txt",typus);
                        } catch (FileNotFoundException e1) {
                            bufor.add("file not found");
                            try {
                                readMaze("mazes/maze-default.txt",0);
                            } catch (FileNotFoundException e2) {
                                e2.printStackTrace();
                            }
                        }
                        paintScene();
                    }
                    else {
                        bufor.add("bad command");
                    }
                }
                bufor.add("simulation > ");
                break;
            default:
                if(Character.isAlphabetic(e.getKeyChar()) || Character.isDigit(e.getKeyChar()) || e.getKeyChar() == KeyEvent.VK_SPACE)
                    bufor.set(bufor.size()-1,bufor.get(bufor.size()-1)+e.getKeyChar());
                break;
        }
        paintScene();
    }

    @Override
    public void keyReleased(KeyEvent e) {}

    @Override
    public void keyTyped(KeyEvent e) {}

}