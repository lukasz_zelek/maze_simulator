import java.util.ArrayList;
import java.awt.Color;
import java.awt.Dimension;

public class MicroMouse {
    int target = -1;
    int N = 16;
    int rotation; //przod na polnoc: 0 na wschod: 1, na poludnie: 2, na zachod: 3
    int X, Y;
    int[] next = {1, 2, 3, 0}, previous = {3, 0, 1, 2};
    char[][] maze;
    int timeout;
    public int[][] mappedArea = new int[16][16];//tablica masek bitowych o postaci [N,E,S,W]
    int[][] zalany_labirynt = new int[16][16];

    private int mmx, mmy, mmr;
    private int righthanded;
    private boolean visited[] = new boolean[256];
    boolean actual_visit_state;
    private int rcor[] = {-16, -1, 16, 1}, lcor[] = {-16, 1, 16, -1};
    int[] ecor = {4, 1}, wcor = {1, 4};

    public MicroMouse(int x, int y, int r, char[][] maze) {
        righthanded = -1;
        mmx = 0;
        mmy = 0;
        mmr = 0;
        this.X = x;
        this.Y = y;
        this.rotation = r;
        this.maze = maze;
        this.timeout = 1000;
        for (int i = 0; i < 16; i++)
            for (int j = 0; j < 16; j++)
                mappedArea[i][j] = 0;
        for (int i = 1; i < 256; i++)
            visited[i] = false;
        visited[0] = true;
        Main.colorPath = new Color(0, 126, 255);
    }

    void poczatkowyStanZalania() {
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                zalany_labirynt[i][j] = 255;
    }

    void zalejLabirynt(ArrayList<Integer> kolejka) {
        while (!kolejka.isEmpty()) {
            int k = kolejka.get(0);
            kolejka.remove(0);
            if (k / N - 1 > -1) {
                if ((mappedArea[k / N][k % N] & 8) != 8) //nie ma scianki na polnocy
                    if (zalany_labirynt[k / N - 1][k % N] == 255) {
                        kolejka.add(k - N);
                        zalany_labirynt[k / N - 1][k % N] = zalany_labirynt[k / N][k % N] + 1;
                    }
            }
            if (k % N + 1 < N)
                if ((mappedArea[k / N][k % N] & ecor[righthanded]) != ecor[righthanded]) //nie ma scianki na wschodzie
                    if (zalany_labirynt[k / N][k % N + 1] == 255) {
                        kolejka.add(k + 1);
                        zalany_labirynt[k / N][k % N + 1] = zalany_labirynt[k / N][k % N] + 1;
                    }
            if (k / N + 1 < N)
                if ((mappedArea[k / N][k % N] & 2) != 2) //nie ma scianki na poludniu
                    if (zalany_labirynt[k / N + 1][k % N] == 255) {
                        kolejka.add(k + N);
                        zalany_labirynt[k / N + 1][k % N] = zalany_labirynt[k / N][k % N] + 1;
                    }
            if (k % N - 1 > -1)
                if ((mappedArea[k / N][k % N] & wcor[righthanded]) != wcor[righthanded]) //nie ma scianki na zachodzie
                    if (zalany_labirynt[k / N][k % N - 1] == 255) {
                        kolejka.add(k - 1);
                        zalany_labirynt[k / N][k % N - 1] = zalany_labirynt[k / N][k % N] + 1;
                    }
        }
    }

    public int getAreaType() {
        return this.righthanded;
    }

    public int getRotation() {
        return this.rotation;
    }

    public int getX() {
        return this.X;
    }

    public int getY() {
        return this.Y;
    }

    public void setTimeout(int millis) {
        this.timeout = millis;
    }

    public void waitAMoment(int millis) {
        try {
            Main.paintScene();
            Thread.sleep(millis);
            Main.paintScene();
        } catch (Exception e) {
        }
    }

    public void goForward() {
        switch (rotation) {
            case 0:
                this.Y--;
                break;
            case 1:
                this.X++;
                break;
            case 2:
                this.Y++;
                break;
            case 3:
                this.X--;
                break;
        }
        switch (mmr) {
            case 0:
                this.mmy--;
                break;
            case 1:
                if (righthanded == 1)
                    this.mmx--;
                else
                    this.mmx++;
                break;
            case 2:
                this.mmy++;
                break;
            case 3:
                if (righthanded == 1)
                    this.mmx++;
                else
                    this.mmx--;
                break;
        }
        waitAMoment(this.timeout);
        actual_visit_state = visited[mmy * 16 + mmx];
        visited[mmy * 16 + mmx] = true;
    }

    public void turnLeft() {
        this.rotation = previous[this.rotation];
        this.mmr = previous[this.mmr];
        waitAMoment(this.timeout * 2);
    }

    public void turnRight() {
        this.rotation = next[this.rotation];
        this.mmr = next[this.mmr];
        waitAMoment(this.timeout * 2);
    }

    public int[][] getMappedArea() {
        return this.mappedArea;
    }

    public int[][] getFlooding() {
        return this.zalany_labirynt;
    }

    public boolean[] getVisited() {
        return this.visited;
    }

    public int getValuesFromSensors() {
        int bitmask;//maska bitowa dla danych z 3 czujników [LEFT,FRONT,RIGHT]
        int t_bitmask = 8 + 4 + 2 + 1;//maska bitowa dla wszyskich 4 kierunkow [N,E,S,W]
        if (maze[2 * this.Y][2 * this.X + 1] == 'o')
            t_bitmask -= 8;
        if (maze[2 * this.Y + 1][2 * this.X + 2] == 'o')
            t_bitmask -= 4;
        if (maze[2 * this.Y + 2][2 * this.X + 1] == 'o')
            t_bitmask -= 2;
        if (maze[2 * this.Y + 1][2 * this.X] == 'o')
            t_bitmask--;
        int t = t_bitmask;
        t_bitmask <<= 4;
        t_bitmask += t;
        if (this.rotation == 0)
            bitmask = t_bitmask >> 2;
        else
            bitmask = t_bitmask >> (6 - this.rotation);
        return bitmask % 8;
    }

    public boolean isWallLeft() {
        if ((getValuesFromSensors() & 4) == 0) return false;
        else return true;
    }

    public boolean isWallFront() {
        if ((getValuesFromSensors() & 2) == 0) return false;
        else return true;
    }

    public boolean isWallRight() {
        if ((getValuesFromSensors() & 1) == 0) return false;
        else return true;
    }

    public void fillWalls(int mx, int my) {
        if (righthanded == 0) {
            if (my - 1 > -1)
                if ((mappedArea[my][mx] & 8) == 8) {
                    mappedArea[my - 1][mx] = (2 | mappedArea[my - 1][mx]);
                }
            if (mx + 1 < 16)
                if ((mappedArea[my][mx] & 4) == 4) {
                    mappedArea[my][mx + 1] = (1 | mappedArea[my][mx + 1]);
                }
            if (my + 1 < 16)
                if ((mappedArea[my][mx] & 2) == 2) {
                    mappedArea[my + 1][mx] = (8 | mappedArea[my + 1][mx]);
                }
            if (mx - 1 > -1)
                if ((mappedArea[my][mx] & 1) == 1) {
                    mappedArea[my][mx - 1] = (4 | mappedArea[my][mx - 1]);
                }
        } else if (righthanded == 1) {
            if (my - 1 > -1)
                if ((mappedArea[my][mx] & 8) == 8) {
                    mappedArea[my - 1][mx] = (2 | mappedArea[my - 1][mx]);
                }
            if (mx + 1 < 16)
                if ((mappedArea[my][mx] & 1) == 1) {
                    mappedArea[my][mx + 1] = (4 | mappedArea[my][mx + 1]);
                }
            if (my + 1 < 16)
                if ((mappedArea[my][mx] & 2) == 2) {
                    mappedArea[my + 1][mx] = (8 | mappedArea[my + 1][mx]);
                }
            if (mx - 1 > -1)
                if ((mappedArea[my][mx] & 4) == 4) {
                    mappedArea[my][mx - 1] = (1 | mappedArea[my][mx - 1]);
                }
        }
    }

    public void maping() {
        int t = getValuesFromSensors();
        int tt = t + (t << 4);
        mappedArea[mmy][mmx] = (mappedArea[mmy][mmx] | ((tt >> next[next[mmr]]) % 16));
        turnLeft();
        turnLeft();
        while (!isWallFront()) {
            Main.PATH.add(new Dimension(getY() * 16 + getX() + 1, getY() * 16 + getX() + Main.corrections[rotation]));
            goForward();
            t = getValuesFromSensors();
            tt = t + (t << 4);
            mappedArea[mmy][mmx] = (mappedArea[mmy][mmx] | ((tt >> next[next[mmr]]) % 16));
            if (!isWallRight()) {
                righthanded = 1;
                break;
            }
            if (!isWallLeft()) {
                righthanded = 0;
                break;
            }
        }
        int mmmy = mmy;
        while (mmmy >= 0) {
            fillWalls(mmx, mmmy);
            mmmy--;
        }
        int tab[] = {0x77, 0x78, 0x87, 0x88};
        while ((mmy * 16 + mmx != tab[0]) && (mmy * 16 + mmx != tab[1]) && (mmy * 16 + mmx != tab[2]) && (mmy * 16 + mmx != tab[3])) {
            poczatkowyStanZalania();
            for (int i = 0; i < 4; i++)
                zalany_labirynt[tab[i] / N][tab[i] % N] = 0;
            t = getValuesFromSensors();
            tt = t + (t << 4);
            mappedArea[mmy][mmx] = (mappedArea[mmy][mmx] | ((tt >> next[next[mmr]]) % 16));
            fillWalls(mmx, mmy);
            ArrayList<Integer> kolejka = new ArrayList<Integer>();
            for (int i = 0; i < 4; i++)
                kolejka.add(tab[i]);
            zalejLabirynt(kolejka);
            floodfill();
        }
        target = mmy * 16 + mmx;

        int next_to_explore = -1;
        for (int i = 0; i <= 0xff; i++) {
            if (visited[i] == true) {
                for (int j = 0; j < 4; j++) {
                    if ((mappedArea[i / N][i % N] & (8 >> j)) == 0) {
                        if (righthanded == 0) {
                            int flval = zalany_labirynt[(i + lcor[j]) / N][(i + lcor[j]) % N];
                            if (flval < zalany_labirynt[i / N][i % N])
                                if (!visited[i + lcor[j]]) {
                                    next_to_explore = i;
                                    break;
                                }
                        } else {
                            int flval = zalany_labirynt[(i + rcor[j]) / N][(i + rcor[j]) % N];
                            if (flval < zalany_labirynt[i / N][i % N])
                                if (!visited[i + rcor[j]]) {
                                    next_to_explore = i;
                                    break;
                                }
                        }
                    }
                }
            }
            if (next_to_explore != -1) break;
        }
        while (next_to_explore != -1) {
            while (mmy * 16 + mmx != next_to_explore) {
                poczatkowyStanZalania();
                zalany_labirynt[next_to_explore / N][next_to_explore % N] = 0;
                t = getValuesFromSensors();
                tt = t + (t << 4);
                mappedArea[mmy][mmx] = (mappedArea[mmy][mmx] | ((tt >> next[next[mmr]]) % 16));
                fillWalls(mmx, mmy);
                ArrayList<Integer> kolejka = new ArrayList<Integer>();
                kolejka.add(next_to_explore);
                zalejLabirynt(kolejka);
                floodfill();
            }
            next_to_explore = -1;
            while ((mmy * 16 + mmx != tab[0]) && (mmy * 16 + mmx != tab[1]) && (mmy * 16 + mmx != tab[2]) && (mmy * 16 + mmx != tab[3])) {
                poczatkowyStanZalania();
                for (int i = 0; i < 4; i++)
                    zalany_labirynt[tab[i] / N][tab[i] % N] = 0;
                t = getValuesFromSensors();
                tt = t + (t << 4);
                mappedArea[mmy][mmx] = (mappedArea[mmy][mmx] | ((tt >> next[next[mmr]]) % 16));
                fillWalls(mmx, mmy);
                ArrayList<Integer> kolejka = new ArrayList<Integer>();
                for (int i = 0; i < 4; i++)
                    kolejka.add(tab[i]);
                zalejLabirynt(kolejka);
                floodfill();
                if (actual_visit_state == true)
                    break;
            }
            for (int i = 0; i <= 0xff; i++) {
                if (visited[i] == true) {
                    for (int j = 0; j < 4; j++) {
                        if ((mappedArea[i / N][i % N] & (8 >> j)) == 0) {
                            if (righthanded == 0) {
                                int flval = zalany_labirynt[(i + lcor[j]) / N][(i + lcor[j]) % N];
                                if (flval < zalany_labirynt[i / N][i % N])
                                    if (!visited[i + lcor[j]]) {
                                        next_to_explore = i;
                                        break;
                                    }
                            } else {
                                int flval = zalany_labirynt[(i + rcor[j]) / N][(i + rcor[j]) % N];
                                if (flval < zalany_labirynt[i / N][i % N])
                                    if (!visited[i + rcor[j]]) {
                                        next_to_explore = i;
                                        break;
                                    }
                            }
                        }
                    }
                }
                if (next_to_explore != -1) break;
            }
        }
        while (mmy * 16 + mmx != 0) {
            poczatkowyStanZalania();
            zalany_labirynt[0][0] = 0;
            t = getValuesFromSensors();
            tt = t + (t << 4);
            mappedArea[mmy][mmx] = (mappedArea[mmy][mmx] | ((tt >> next[next[mmr]]) % 16));
            fillWalls(mmx, mmy);
            ArrayList<Integer> kolejka = new ArrayList<Integer>();
            kolejka.add(0);
            zalejLabirynt(kolejka);
            floodfill();
        }
        Main.mapingLength = Main.PATH.size();
        Main.PATH.clear();
        Main.colorPath = new Color(53, 255, 2);
        poczatkowyStanZalania();
        for (int i = 0; i < 4; i++)
            zalany_labirynt[tab[i] / N][tab[i] % N] = 0;
        t = getValuesFromSensors();
        tt = t + (t << 4);
        mappedArea[mmy][mmx] = (mappedArea[mmy][mmx] | ((tt >> next[next[mmr]]) % 16));
        fillWalls(mmx, mmy);
        ArrayList<Integer> kolejka = new ArrayList<Integer>();
        for (int i = 0; i < 4; i++)
            kolejka.add(tab[i]);
        zalejLabirynt(kolejka);
        goFastestWay();
    }

    public void floodfill() {
        if (!Main.simulation) return;
        int k = -1, b = -1;
        for (int i = 0; i < 4; i++) {
            if ((mappedArea[mmy][mmx] & (8 >> i)) == 0)
                if (righthanded == 0) {
                    int flval = zalany_labirynt[(mmy * 16 + mmx + lcor[i]) / 16][(mmy * 16 + mmx + lcor[i]) % 16];
                    if (flval == zalany_labirynt[mmy][mmx]) {
                        if (b < 0) {
                            b = 0;
                            k = i;
                        } else if (b == 0)
                            if (visited[mmy * 16 + mmx + lcor[i]] == false)
                                k = i;
                    } else if (flval < zalany_labirynt[mmy][mmx]) {
                        if (b < 1) {
                            b = 1;
                            k = i;
                        } else if (b == 1)
                            if (visited[mmy * 16 + mmx + lcor[i]] == false)
                                k = i;
                    }
                } else {
                    int flvar = zalany_labirynt[(mmy * 16 + mmx + rcor[i]) / 16][(mmy * 16 + mmx + rcor[i]) % 16];
                    if (flvar == zalany_labirynt[mmy][mmx]) {
                        if (b < 0) {
                            b = 0;
                            k = i;
                        } else if (b == 0)
                            if (visited[mmy * 16 + mmx + rcor[i]] == false)
                                k = i;
                    } else if (flvar < zalany_labirynt[mmy][mmx]) {
                        if (b < 1) {
                            b = 1;
                            k = i;
                        } else if (b == 1)
                            if (visited[mmy * 16 + mmx + rcor[i]] == false)
                                k = i;
                    }
                }
        }
        if (k == -1) return;
        correctRotation(mmr, k);
        Main.PATH.add(new Dimension(getY() * 16 + getX() + 1, getY() * 16 + getX() + Main.corrections[rotation]));
        goForward();
    }

    public void goFastestWay() {
        int tab[] = {0x77, 0x78, 0x87, 0x88};
        while ((mmy * 16 + mmx != tab[0]) && (mmy * 16 + mmx != tab[1]) && (mmy * 16 + mmx != tab[2]) && (mmy * 16 + mmx != tab[3])) {
            for (int i = 0; i < 4; i++)
                zalany_labirynt[tab[i] / N][tab[i] % N] = 0;
            floodfill();
        }
    }

    public void correctRotation(int actual, int desired) {
        switch (actual - desired) {
            case -3:
                turnLeft();
                break;
            case -2:
                turnLeft();
                turnLeft();
                break;
            case -1:
                turnRight();
                break;
            case 1:
                turnLeft();
                break;
            case 2:
                turnLeft();
                turnLeft();
                break;
            case 3:
                turnRight();
                break;
        }
    }
}