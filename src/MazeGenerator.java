import java.awt.*;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;

public class MazeGenerator {

    //konwertuje labirynt na mape
    public char[][] convertMaze(char maze[][]) {
        char[] pattern = {'+', '3', '2', '5', '0', 'F', '7', '9', '1', '4', 'E', '8', '6', 'A', 'B'};
        char[][] map = new char[16][16];
        for (int i = 1; i < 33; i += 2)
            for (int j = 1; j < 33; j += 2) {
                int bit_mask = 0; //NESW 0000
                if (maze[i - 1][j] == '-' || maze[i - 1][j] == '#') {
                    bit_mask += (1 << 3);
                }
                if (maze[i][j + 1] == '|' || maze[i][j + 1] == '*') {
                    bit_mask += (1 << 2);
                }
                if (maze[i + 1][j] == '-' || maze[i + 1][j] == '#') {
                    bit_mask += 2;
                }
                if (maze[i][j - 1] == '|' || maze[i][j - 1] == '*') {
                    bit_mask++;
                }
                map[i / 2][j / 2] = pattern[bit_mask];
            }
        return map;
    }

    //konwertuje mape na labirynt
    public char[][] convertMap(char map[][]) {
        char[][] maze = new char[33][33];
        for (int i = 1; i < 33; i += 2)
            for (int j = 1; j < 33; j += 2) {
                switch (map[i/2][j/2])
                {
                    case '+': break;
                    case '1':break;
                    case '2':break;
                    case '3':break;
                    case '4':break;
                    case '5':break;
                    case '6':break;
                    case '7':break;
                    case '8':break;
                    case '9':break;
                    case 'A':break;
                    case 'B':break;
                    case 'E':break;
                    case 'F':break;
                }
            }
        return maze;
    }

    //generuje losowy labirynt algorytmem Prima
    public char[][] generate() {
        char maze[][] = new char[33][33];
        for (int i = 0; i < 33; i += 2) {
            for (int j = 0; j < 33; j += 2)
                maze[i][j] = '.';
        }
        for (int i = 1; i < 33; i += 2) {
            for (int j = 1; j < 33; j += 2)
                maze[i][j] = 'x';
        }
        for (int i = 2; i < 32; i += 2) {
            for (int j = 1; j < 33; j += 2)
                maze[i][j] = '-';
        }
        for (int i = 1; i < 33; i += 2) {
            for (int j = 2; j < 32; j += 2)
                maze[i][j] = '|';
        }
        for (int j = 0; j < 33; j += 32) {
            for (int i = 1; i < 33; i += 2)
                maze[i][j] = '*';
        }
        for (int i = 0; i < 33; i += 32) {
            for (int j = 1; j < 33; j += 2)
                maze[i][j] = '#';
        }
        //kwadrat 2x2 ustawiony na sztywno
        maze[14][14] = '.';
        maze[14][15] = '-';
        maze[14][16] = '.';
        maze[14][17] = '-';
        maze[14][18] = '.';
        maze[15][14] = '|';
        maze[15][15] = 'o';
        maze[15][16] = 'o';
        maze[15][17] = 'o';
        maze[15][18] = '|';
        maze[16][14] = '.';
        maze[16][15] = 'o';
        maze[16][16] = 'o';
        maze[16][17] = 'o';
        maze[16][18] = '.';
        maze[17][14] = '|';
        maze[17][15] = 'o';
        maze[17][16] = 'o';
        maze[17][17] = 'x';
        maze[17][18] = '|';
        maze[18][14] = '.';
        maze[18][15] = '-';
        maze[18][16] = '.';
        maze[18][17] = 'o';
        maze[18][18] = '.';

        ArrayList<Dimension> lista = new ArrayList<Dimension>();
        lista.add(new Dimension(1, 2));
        lista.add(new Dimension(2, 1));
        maze[1][1] = 'o';
        Random r = new Random();
        while (!lista.isEmpty()) {
            int k = r.nextInt(lista.size());
            int y = lista.get(k).height;
            int x = lista.get(k).width;
            if (maze[y][x] == '-') {
                if (maze[y - 1][x] == 'o') {
                    if (maze[y + 1][x] == 'x') {
                        if (maze[y + 2][x] == '-') {
                            if (maze[y + 3][x] == 'x')
                                lista.add(new Dimension(x, y + 2));
                        }
                        if (maze[y + 1][x + 1] == '|') {
                            if (maze[y + 1][x + 2] == 'x')
                                lista.add(new Dimension(x + 1, y + 1));
                        }
                        if (maze[y + 1][x - 1] == '|') {
                            if (maze[y + 1][x - 2] == 'x')
                                lista.add(new Dimension(x - 1, y + 1));
                        }
                        maze[y + 1][x] = 'o';
                        maze[y][x] = 'o';
                    }
                }
                if (maze[y - 1][x] == 'x') {
                    if (maze[y + 1][x] == 'o') {
                        if (maze[y - 2][x] == '-') {
                            if (maze[y - 3][x] == 'x')
                                lista.add(new Dimension(x, y - 2));
                        }
                        if (maze[y - 1][x + 1] == '|') {
                            if (maze[y + 1][x + 2] == 'x')
                                lista.add(new Dimension(x + 1, y - 1));
                        }
                        if (maze[y - 1][x - 1] == '|') {
                            if (maze[y + 1][x - 2] == 'x')
                                lista.add(new Dimension(x - 1, y - 1));
                        }
                        maze[y - 1][x] = 'o';
                        maze[y][x] = 'o';
                    }
                }
            }
            if (maze[y][x] == '|') {
                if (maze[y][x - 1] == 'o') {
                    if (maze[y][x + 1] == 'x') {
                        if (maze[y][x + 2] == '|') {
                            if (maze[y][x + 3] == 'x')
                                lista.add(new Dimension(x + 2, y));
                        }
                        if (maze[y + 1][x + 1] == '-') {
                            if (maze[y + 2][x + 1] == 'x')
                                lista.add(new Dimension(x + 1, y + 1));
                        }
                        if (maze[y - 1][x + 1] == '-') {
                            if (maze[y - 2][x + 1] == 'x')
                                lista.add(new Dimension(x + 1, y - 1));
                        }
                        maze[y][x + 1] = 'o';
                        maze[y][x] = 'o';
                    }
                }
                if (maze[y][x - 1] == 'x') {
                    if (maze[y][x + 1] == 'o') {
                        if (maze[y][x - 2] == '|') {
                            if (maze[y][x - 3] == 'x')
                                lista.add(new Dimension(x - 2, y));
                        }
                        if (maze[y + 1][x - 1] == '-') {
                            if (maze[y + 2][x - 1] == 'x')
                                lista.add(new Dimension(x - 1, y + 1));
                        }
                        if (maze[y - 1][x - 1] == '-') {
                            if (maze[y - 2][x - 1] == 'x')
                                lista.add(new Dimension(x - 1, y - 1));
                        }
                        maze[y][x - 1] = 'o';
                        maze[y][x] = 'o';
                    }
                }
            }
            lista.remove(k);
        }
        //korekta kwadratu 2x2
        maze[14][14] = '.';
        maze[14][15] = '-';
        maze[14][16] = '.';
        maze[14][17] = '-';
        maze[14][18] = '.';
        maze[15][14] = '|';
        maze[15][15] = 'o';
        maze[15][16] = 'o';
        maze[15][17] = 'o';
        maze[15][18] = '|';
        maze[16][14] = '.';
        maze[16][15] = 'o';
        maze[16][16] = 'o';
        maze[16][17] = 'o';
        maze[16][18] = '.';
        maze[17][14] = '|';
        maze[17][15] = 'o';
        maze[17][16] = 'o';
        maze[17][17] = 'o';
        maze[17][18] = '|';
        maze[18][14] = '.';
        maze[18][15] = '-';
        maze[18][16] = '.';
        maze[18][17] = 'o';
        maze[18][18] = '.';
        return maze;
    }

    public static void main(String[] args) throws FileNotFoundException {
        char[][] maze = new MazeGenerator().generate();
        //zapis do pliku
        PrintWriter printWriter = new PrintWriter("mazes/maze-generated.txt");
        for (int i = 0; i < 33; i++){
            for (int j = 0; j < 33; j++)
                printWriter.print(maze[i][j]);
            printWriter.println("");
        }
        printWriter.close();
    }
}
