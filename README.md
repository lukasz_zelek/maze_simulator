# maze_simulator



## Getting started
Program allows you to simulate graph path finding algorithms with graphical representation.
The simulation allows you to enter a maze from a text file.


## Project structure

Project contains Java code in src/ directory. 
Example mazes are located in maz.

## Program View

![](screenview.png)